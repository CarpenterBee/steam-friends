//
//  UINavigationController+Extensions.swift
//  Steam Friends
//
//  Created by Роман Степанов on 10.04.2024.
//

import UIKit

extension UINavigationController {
    
    func setupBarColor() {
        let appearance = UINavigationBarAppearance()
        appearance.configureWithOpaqueBackground()
        
        appearance.backgroundColor = .steamBlackBrown
        appearance.titleTextAttributes = [NSMutableAttributedString.Key.foregroundColor: UIColor.white]
        
        self.navigationBar.standardAppearance = appearance
        self.navigationBar.scrollEdgeAppearance = appearance
        self.navigationBar.compactAppearance = appearance
        
        self.navigationBar.tintColor = .white
    }
    
}
