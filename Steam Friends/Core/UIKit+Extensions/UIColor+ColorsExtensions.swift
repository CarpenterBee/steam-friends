//
//  UIColor+ColorsExtensions.swift
//  Steam Friends
//
//  Created by Роман Степанов on 08.04.2024.
//

import UIKit

public extension UIColor {
    
    static let steamBlackBrown = UIColor(
        red: 32/255,
        green: 33/255,
        blue: 38/255,
        alpha: 1
    )
    
    static let steamBlue = UIColor(
        red: 30/255,
        green: 65/255,
        blue: 95/255,
        alpha: 1
    )
    
    static let steamDarckBlue = UIColor(
        red: 29/256,
        green: 39/256,
        blue: 48/256,
        alpha: 1
    )
    
    static let steamGreen = UIColor(
        red: 162/256,
        green: 204/256,
        blue: 17/256,
        alpha: 1
    )
    
    static let steamLightGreen = UIColor(
        red: 232/256,
        green: 233/256,
        blue: 236/256,
        alpha: 1
    )
    
    static let steamTurquoise = UIColor(
        red: 91/256,
        green: 199/256,
        blue: 217/256,
        alpha: 1
    )
    
    static let authenticateGreen = UIColor(
        red: 148/256,
        green: 187/256,
        blue: 5/256,
        alpha: 1
    )
    
    static let authenticateViolet = UIColor(
        red: 42/256,
        green: 75/256,
        blue: 105/256,
        alpha: 1
    )
}
