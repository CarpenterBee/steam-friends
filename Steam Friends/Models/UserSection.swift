//
//  UserSection.swift
//  Steam Friends
//
//  Created by Роман Степанов on 11.04.2024.
//

import Foundation

public struct UserSection {
    let title: String
    let players: [Player]
}
