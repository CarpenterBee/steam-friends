//
//  AuthenticateModel.swift
//  Steam Friends
//
//  Created by Роман Степанов on 10.04.2024.
//

import Foundation

public struct AuthenticateModel: Codable {
    let key: String
    let id: String
}
