//
//  AuthenticateViewController.swift
//  Steam Friends
//
//  Created by Роман Степанов on 10.04.2024.
//
import UIKit

public protocol AuthenticateViewOutput: AnyObject {
    
    /// Открываем экран, если есть сохраненный пользователь
    /// - Parameters:
    ///   - user: Данные юзера
    func openFriendList(for user: AuthenticateModel?)
}

final class AuthenticateViewController: UIViewController {
    
    var output: AuthenticateViewInput?
    
    // MARK: - Private Properties
    
    private struct Constants {
        static let backgroundImageRightAnchor: CGFloat = 100.0
        static let backgroundImageLeftAnchor: CGFloat = -100.0
        
        static let keyLableFontSize: CGFloat = 18.0
        static let keyLableTopAnchor: CGFloat = 40.0
        static let keyLableLeftAnchor: CGFloat = 20.0
        
        static let layer: CGFloat = 5.0
        
        static let keyTextfieldRightAnchor: CGFloat = -20.0
        static let keyTextfieldLeftAnchor: CGFloat = 20.0
        static let keyTextfieldHeightAnchor: CGFloat = 40.0
        
        static let idTextfieldRightAnchor: CGFloat = -20.0
        static let idTextfieldLeftAnchor: CGFloat = 20.0
        static let idTextfieldHeightAnchor: CGFloat = 40.0
        
        static let idLableFontSize: CGFloat = 18.0
        static let idLableLeftAnchor: CGFloat = 20.0
        static let idLableTopAnchor: CGFloat = 30.0
        
        static let loginButtonTopAnchor: CGFloat = 50.0
        static let loginButtonLeftAnchor: CGFloat = 20.0
        static let loginButtonRightAnchor: CGFloat = -20.0
        static let loginButtonHeightAnchor: CGFloat = 40.0
        
        static let containerTopAnchor: CGFloat = 140.0
    }
    
    private let container = UIView()
    private let keyLable = UILabel()
    private let idLable = UILabel()
    private let keyTextfield = UITextField()
    private let idTextfield = UITextField()
    private let loginButton = UIButton()
    private let backgroundImage = UIImageView()
    
    public init(output: AuthenticateViewInput? = nil) {
        super.init(nibName: nil, bundle: nil)
        self.output = output
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        output?.viewDidLoad()
        self.view.backgroundColor = .steamDarckBlue
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Steam"
    }
    
    // MARK: - Private Methods
    
    private func setupUI() {
        setupBackground()
        setupContainer()
        setupKeyLable()
        setupKeyTextfield()
        setupIdLable()
        setupIdTextfield()
        setupLoginButton()
    }
    
    private func setupBackground() {
        view.addSubview(backgroundImage)
        backgroundImage.image = UIImage(named: "back")
        backgroundImage.contentMode = .scaleAspectFill
        
        backgroundImage.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            backgroundImage.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor, constant: Constants.backgroundImageLeftAnchor),
            backgroundImage.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor, constant: Constants.backgroundImageRightAnchor),
            backgroundImage.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            backgroundImage.bottomAnchor.constraint(equalTo: view.bottomAnchor),
        ])
    }
    
    private func setupContainer() {
        view.addSubview(container)
        
        container.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            container.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor),
            container.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor),
            container.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: Constants.containerTopAnchor),
        ])
    }
    
    private func setupKeyLable() {
        container.addSubview(keyLable)
        keyLable.text = "Steam key"
        keyLable.font = .systemFont(ofSize: Constants.keyLableFontSize, weight: .light)
        keyLable.textColor = .white
        
        keyLable.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            keyLable.leftAnchor.constraint(equalTo: container.leftAnchor, constant: Constants.keyLableLeftAnchor),
            keyLable.topAnchor.constraint(equalTo: container.topAnchor, constant: Constants.keyLableTopAnchor),
        ])
    }
    
    private func setupKeyTextfield() {
        container.addSubview(keyTextfield)
        keyTextfield.backgroundColor = .gray
        keyTextfield.layer.cornerRadius = Constants.layer
        
        keyTextfield.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            keyTextfield.leftAnchor.constraint(equalTo: container.leftAnchor, constant: Constants.keyTextfieldLeftAnchor),
            keyTextfield.rightAnchor.constraint(equalTo: container.rightAnchor, constant: Constants.keyTextfieldRightAnchor),
            keyTextfield.topAnchor.constraint(equalTo: keyLable.bottomAnchor),
            keyTextfield.heightAnchor.constraint(equalToConstant: Constants.keyTextfieldHeightAnchor),
        ])
    }
    
    private func setupIdLable() {
        container.addSubview(idLable)
        idLable.text = "Steam id"
        idLable.font = .systemFont(ofSize: Constants.idLableFontSize, weight: .light)
        idLable.textColor = .white
        
        idLable.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            idLable.leftAnchor.constraint(equalTo: container.leftAnchor, constant: Constants.idLableLeftAnchor),
            idLable.topAnchor.constraint(equalTo: keyTextfield.bottomAnchor, constant: Constants.idLableTopAnchor),
        ])
    }
    
    private func setupIdTextfield() {
        container.addSubview(idTextfield)
        idTextfield.backgroundColor = .gray
        idTextfield.layer.cornerRadius = Constants.layer
        
        idTextfield.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            idTextfield.leftAnchor.constraint(equalTo: container.leftAnchor, constant: Constants.idTextfieldLeftAnchor),
            idTextfield.rightAnchor.constraint(equalTo: container.rightAnchor, constant: Constants.idTextfieldRightAnchor),
            idTextfield.topAnchor.constraint(equalTo: idLable.bottomAnchor),
            idTextfield.heightAnchor.constraint(equalToConstant: Constants.idTextfieldHeightAnchor),
        ])
    }
    
    private func setupLoginButton() {
        loginButton.addTarget(self, action: #selector(showFrends), for: .touchUpInside)
        
        container.addSubview(loginButton)
        loginButton.setTitle("Sing in", for: .normal)
        loginButton.backgroundColor = .blue
        loginButton.layer.cornerRadius = Constants.layer

        loginButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            loginButton.topAnchor.constraint(equalTo: idTextfield.bottomAnchor, constant: Constants.loginButtonTopAnchor),
            loginButton.leftAnchor.constraint(equalTo: container.leftAnchor, constant: Constants.loginButtonLeftAnchor),
            loginButton.rightAnchor.constraint(equalTo: container.rightAnchor, constant: Constants.loginButtonRightAnchor),
            loginButton.bottomAnchor.constraint(equalTo: container.bottomAnchor),
            loginButton.heightAnchor.constraint(equalToConstant: Constants.loginButtonHeightAnchor),
        ])
    }
    
    @objc private func showFrends() {
        let model = AuthenticateModel(key: keyTextfield.text ?? "", id: idTextfield.text ?? "")
        output?.saveUserCredentials(model: model)
        output?.showUserFrends(model: model)
    }
    
}

extension AuthenticateViewController: AuthenticateViewOutput {
    func openFriendList(for user: AuthenticateModel?) {
        guard let user else { return }
        output?.showUserFrends(model: user)
    }
}
