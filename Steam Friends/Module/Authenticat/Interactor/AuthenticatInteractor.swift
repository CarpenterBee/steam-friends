//
//  AuthenticatInteractor.swift
//  Steam Friends
//
//  Created by Роман Степанов on 10.04.2024.
//

import Foundation

public protocol AuthenticateInteractorInterface: AnyObject {
    
    /// Достать данные пользователя с помощью UserDefaults
    /// - Parameters:
    ///   - key: Ключ
    ///   - completion: Выполнится блок кода, в который передадим полученные данные
    func retrieveUser(with key: UserDefaultsKey, completion: @escaping (AuthenticateModel?) -> Void)
    
    /// Сохранить данные пользователя с помощью UserDefaults
    /// - Parameters:
    ///   - value: Моделька с ключом и id
    ///   - key: Ключ
    func saveUser(value: AuthenticateModel, key: UserDefaultsKey)
    
    /// Удалить все данные в UserDefaults по заданному ключу
    /// - Parameters:
    ///   - key: Ключ
    func removeData(for key: UserDefaultsKey)
}

public final class AuthenticateInteractor: AuthenticateInteractorInterface {
    
    private let userDefaults = UserDefaults.standard
    
    public func retrieveUser(with key: UserDefaultsKey, completion: @escaping (AuthenticateModel?) -> Void) {
        if let data = userDefaults.object(forKey: key.rawValue) as? Data,
           let userCredentials = try? JSONDecoder().decode(AuthenticateModel.self, from: data) {
            completion(userCredentials)
        }
    }
    
    public func saveUser(value: AuthenticateModel, key: UserDefaultsKey) {
        if let encoded = try? JSONEncoder().encode(value) {
            userDefaults.set(encoded, forKey: key.rawValue)
        }
    }
    
    public func removeData(for key: UserDefaultsKey) {
        userDefaults.removeObject(forKey: key.rawValue)
    }
}

public enum UserDefaultsKey: String {
    case kUserCredentials
}
