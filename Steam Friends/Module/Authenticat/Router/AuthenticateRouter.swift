//
//  AuthenticateRouter.swift
//  Steam Friends
//
//  Created by Роман Степанов on 10.04.2024.
//

protocol AuthenticateRouterProtocol: AnyObject {
    
    func openFriendsList(model: AuthenticateModel)
    
}

final class AuthenticateRouter: AuthenticateRouterProtocol {
    
    weak var view: AuthenticateViewController?
    
    public func openFriendsList(model: AuthenticateModel) {
        
        let module = SteamFriendsModuleBuilder.build(model: model)
        view?.navigationController?.pushViewController(module, animated: true)
    }
}
