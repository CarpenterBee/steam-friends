//
//  AuthenticatPresenter.swift
//  Steam Friends
//
//  Created by Роман Степанов on 10.04.2024.
//

import UIKit

protocol AuthenticateViewInput: AnyObject {
    
    /// Вызываем при первой загрузке view
    func viewDidLoad()
    
    /// Показываем друзей для конкретного пользователя
    /// - Parameters:
    ///   - model: Модель данных пользователя
    func showUserFrends(model: AuthenticateModel)
    
    /// Сохраняем введенные данные пользователя
    /// - Parameters:
    ///   - model: Модель данных пользователя
    func saveUserCredentials(model: AuthenticateModel)
    
    /// Пытаемся достать сохраненные данные пользователя
    func tryRetreiveUser()
}

final class AuthenticatePresenter {

    var view: AuthenticateViewOutput?
    private var router: AuthenticateRouterProtocol
    private var interactor: AuthenticateInteractorInterface
    
    init(interactor: AuthenticateInteractorInterface = AuthenticateInteractor(),
         router: AuthenticateRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }
    
}

extension AuthenticatePresenter: AuthenticateViewInput {
    
    public func showUserFrends(model: AuthenticateModel) {
        router.openFriendsList(model: model)
    }
    
    func saveUserCredentials(model: AuthenticateModel) {
        interactor.saveUser(value: model, key: .kUserCredentials)
    }
    
    public func tryRetreiveUser() {
        interactor.retrieveUser(with: .kUserCredentials) { [weak self] userCredentials in
            self?.view?.openFriendList(for: userCredentials)
        }
    }
    
    public func viewDidLoad() {
        tryRetreiveUser()
    }
    
}
