//
//  AuthenticateModuleBuilder.swift
//  Steam Friends
//
//  Created by Роман Степанов on 10.04.2024.
//
import Foundation

final class AuthenticateModuleBuilder {
    
    static func build() -> AuthenticateViewController {
        let interactor = AuthenticateInteractor()
        let router = AuthenticateRouter()
        let presenter = AuthenticatePresenter(interactor: interactor, router: router)
        let viewController = AuthenticateViewController(output: presenter)
        presenter.view = viewController
        router.view = viewController
        
        return viewController
    }
}

