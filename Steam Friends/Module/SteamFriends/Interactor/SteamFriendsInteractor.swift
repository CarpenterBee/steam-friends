//
//  SteamFriendsInteractor.swift 
//  Steam Friends
//
//  Created by Роман Степанов on 08.04.2024.
//

import Foundation
import UIKit

protocol FriendsInteractorProtocol {
    
    func getFriendsInfo(completion: @escaping (([Player]) -> Void))
    
    func getProfileImage(completion: @escaping (UIImage) -> Void)
}

final class SteamFriendsInteractor: FriendsInteractorProtocol {
    
    private let friendsService: FriendsServiceProtocol
    private let authenticateModel: AuthenticateModel
    
    public init(friendsService: FriendsServiceProtocol = FriendsService(), model: AuthenticateModel) {
        self.friendsService = friendsService
        self.authenticateModel = model
    }
    
    func getFriendsInfo(completion: @escaping ([Player]) -> Void) {
        friendsService.getFriends(userModel: authenticateModel) { friends in
            completion(friends)
        }
    }
    
    func getProfileImage(completion: @escaping (UIImage) -> Void) {
        friendsService.getProfileImage(userModel: authenticateModel) { image in
            completion(image)
        }
    }
}
