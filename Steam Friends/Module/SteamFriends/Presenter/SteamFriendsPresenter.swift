//
//  SteamFriendsPresenter.swift
//  Steam Friends
//
//  Created by Роман Степанов on 08.04.2024.
//

import Foundation

protocol FriendsViewInput {
    
    func viewDidLoad()
 
    func setFriends()
}

final class SteamFriendsPresenter: FriendsViewInput {
    
    var view: FriendsViewOutput?
    
    var userdata: [Player] = [] {
        didSet {
            self.view?.update(userdata)
        }
    }
    
    private let interactor: FriendsInteractorProtocol
    
    public init(interactor: FriendsInteractorProtocol) {
        self.interactor = interactor
    }
    
    func viewDidLoad() {
        setFriends()
        setProfile()
    }
    
    func setFriends() {
        interactor.getFriendsInfo { friends in
            self.view?.update(friends)
        }
    }
    
    func setProfile() {
        interactor.getProfileImage { image in
            self.view?.setupIcon(image)
        }
    }
}
