//
//  PlayerSummaries.swift
//  Steam Friends
//
//  Created by Роман Степанов on 08.04.2024.
//

import Foundation

public struct UserInfo: Codable {
    let users: Players
    
    enum CodingKeys: String, CodingKey {
        case users = "response"
    }
}

public struct Players: Codable {
    let players: [Player]
}

public struct Player: Codable {
    let personaname: String
    let avatarfull, avatar: String
    let lastlogoff: Int?
    let gameextrainfo: String?
    let personastate: Int
}
