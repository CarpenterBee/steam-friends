//
//  FrendsList.swift
//  Steam Friends
//
//  Created by Роман Степанов on 08.04.2024.
//

import Foundation

struct Friendslist: Codable {
    let friendslist: Friends
}

// MARK: - Friendslist
struct Friends: Codable {
    let friends: [Friend]
}

// MARK: - Friend
struct Friend: Codable {
    let steamid: String
}

enum Relationship: String, Codable {
    case friend = "friend"
}
