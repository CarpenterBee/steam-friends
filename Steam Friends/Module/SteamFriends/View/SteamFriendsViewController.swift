//
//  SteamFriendsViewController.swift
//  Steam Friends
//
//  Created by Роман Степанов on 08.04.2024.
//

import UIKit

protocol FriendsViewOutput {
    func update(_ userInfo: [Player])
    
    func setupIcon(_ image: UIImage)
}

class SteamFriendsViewController: UIViewController {
    
    var output: FriendsViewInput?
    
    // MARK: - Private Properties
    
    private struct Constants {
        static let screenTitle = "Your Friends"
        static let cellID = String(describing: FriendsTibleViewCell.self)
        static let cellHeight: CGFloat = 80.0
        static let labelFontSize: CGFloat = 18.0
    }
    
    private let profileIcon = UIImageView()
    private let friendsTableView = UITableView(frame: .zero, style: .grouped)
    
    private var usersSections: [UserSection] = [] {
        didSet {
            friendsTableView.reloadData()
        }
    }
    
    // MARK: - Init
    
    public init(output: FriendsViewInput? = nil) {
        super.init(nibName: nil, bundle: nil)
        self.output = output
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .steamBlue
        output?.viewDidLoad()
        setupUI()
    }
    
    // MARK: - Private Methods
    
    private func setupUI() {
        setupTableView()
        configureNavigationController()
        friendsTableView.register(FriendsTibleViewCell.self, forCellReuseIdentifier: Constants.cellID)
    }
    
    private func setupTableView() {
        view.addSubview(friendsTableView)
        friendsTableView.backgroundColor = .steamBlue
        friendsTableView.dataSource = self
        friendsTableView.delegate = self
        friendsTableView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            friendsTableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            friendsTableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            friendsTableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            friendsTableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
        ])
    }
    
    private func configureNavigationController() {
        navigationItem.title = Constants.screenTitle
    }
}

extension SteamFriendsViewController: UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        usersSections[section].players.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        usersSections.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.cellID,
                                                 for: indexPath) as! FriendsTibleViewCell
        let profile = usersSections[indexPath.section].players[indexPath.row]
        cell.updateCell(profile)
        
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        Constants.cellHeight
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let label = UILabel()
        
        label.font = UIFont.systemFont(ofSize: Constants.labelFontSize , weight: .light)
        label.textColor = .white
        label.textAlignment = .left
        label.text = "  \(usersSections[section].title)"
        return label
    }
}
extension SteamFriendsViewController: FriendsViewOutput {
    func update(_ userInfo: [Player]) {
        var offlineUsers: [Player] = []
        var onlineUsers: [Player] = []
        var playingUsers: [Player] = []
        for user in userInfo {
            switch user.personastate {
            case 0:
                offlineUsers.append(user)
            case 1...6:
                if user.gameextrainfo != nil {
                    playingUsers.append(user)
                } else {
                    onlineUsers.append(user)
                }
            default: break
            }
        }
        usersSections = [
            .init(title: "Playing (\(playingUsers.count))", players: playingUsers),
            .init(title: "Online (\(onlineUsers.count))", players: onlineUsers),
            .init(title: "Offline (\(offlineUsers.count))", players: offlineUsers),
        ]
    }
    
    func setupIcon(_ image: UIImage) {
        let images = UIImageView()
        images.image = image
        
        navigationItem.rightBarButtonItem = UIBarButtonItem.init(customView: images)
    }
    
}
