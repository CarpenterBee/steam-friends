//
//  FriendsTibleViewCell.swift
//  Steam Friends
//
//  Created by Роман Степанов on 08.04.2024.
//

import UIKit

class FriendsTibleViewCell: UITableViewCell {
    
    // MARK: - Private Properties
    
    private struct Constants {
        static let titleFontSize: CGFloat = 14.0
        static let subtitleFontSize: CGFloat = 12.0
        
        static let titleLeadingAnchor: CGFloat = 10.0
        static let titleTopAnchor: CGFloat = 10.0
        
        static let subtitleLeadingAnchor: CGFloat = 10.0
        static let subtitleTopAnchor: CGFloat = 5.0
        
        static let userStatusWidth: CGFloat = 5.0
        
        static let containerLeftAnchor: CGFloat = 10.0
        static let containerTopAnchor: CGFloat = 10.0
        static let containerBottomAnchor: CGFloat = -10.0
        static let containerRightAnchor: CGFloat = -30.0
        
        static let imageSize: CGFloat = 60.0
    }
    
    private let userAvatar = UIImageView()
    private let title = UILabel()
    private let subtitle = UILabel()
    private let container = UIView()
    private let userStatusView = UIView()
    
    private let webImageManager: WebImageServiceProtocol = WebImageService()
    
    // MARK: - Init
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    // MARK: - Public Methods
    
    public func updateCell(_ userInfo: Player) {
        setupImage(userInfo.avatarfull)
        title.text = userInfo.personaname
        
        var state: String?
        let data = Date(timeIntervalSince1970: TimeInterval(userInfo.lastlogoff ?? 10000000))
        let timestamp = DateFormatter.localizedString(
            from: data,
            dateStyle: .medium,
            timeStyle: .short
        )
        
        setupStyle(type: .online)
        switch userInfo.personastate {
        case 0:
            state = "Last Online: \(timestamp)"
            setupStyle(type: .offline)
        case 1:
            setupStyle(type: .online)
        case 2:
            state = "Busy"
        case 3:
            state = "Away"
        case 4:
            state = "Snooze"
        case 5:
            state = "Looking to trade"
        case 6:
            state = "Looking to play"
        default:
            break
        }
        
        if userInfo.gameextrainfo != nil {
            state = userInfo.gameextrainfo
            setupStyle(type: .isPlaying)
        }
        
        subtitle.text = ("\(state ?? "")")
    }
    
    public func setupStyle(type: UserStatus) {
        var color: UIColor = .white
        switch type {
        case .isPlaying:
            color = .steamGreen
        case .offline:
            color = .gray
        case .online:
            color = .steamTurquoise
        }
        
        userStatusView.backgroundColor = color
        title.textColor = color
        subtitle.textColor = color
    }
    
    // MARK: - Private Methods
    
    private func setupUI() {
        self.backgroundColor = .steamBlue
        setupContainer()
        setupUserAvatar()
        setupStatusView()
        setupTitle()
        setupSubtitle()
    }
    
    private func setupContainer() {
        contentView.addSubview(container)
        container.backgroundColor = .steamDarckBlue
        
        container.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            container.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: Constants.containerLeftAnchor),
            container.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: Constants.containerRightAnchor),
            container.topAnchor.constraint(equalTo: contentView.topAnchor, constant: Constants.containerTopAnchor),
            container.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: Constants.containerBottomAnchor),
        ])
    }
    
    private func setupUserAvatar() {
        userAvatar.contentMode = .scaleAspectFill
        userAvatar.backgroundColor = .red
        container.addSubview(userAvatar)
        
        userAvatar.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            userAvatar.centerYAnchor.constraint(equalTo: container.centerYAnchor),
            userAvatar.leadingAnchor.constraint(equalTo: container.leadingAnchor),
            userAvatar.widthAnchor.constraint(equalToConstant: Constants.imageSize),
            userAvatar.heightAnchor.constraint(equalToConstant: Constants.imageSize)
        ])
    }
    
    private func setupStatusView() {
        container.addSubview(userStatusView)
        
        userStatusView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            userStatusView.rightAnchor.constraint(equalTo: userAvatar.rightAnchor),
            userStatusView.widthAnchor.constraint(equalToConstant: Constants.userStatusWidth),
            userStatusView.topAnchor.constraint(equalTo: container.topAnchor),
            userStatusView.bottomAnchor.constraint(equalTo: container.bottomAnchor),
        ])
    }
    
    private func setupTitle() {
        title.font = UIFont.boldSystemFont(ofSize: Constants.titleFontSize)
        
        container.addSubview(title)
        title.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            title.topAnchor.constraint(equalTo: container.topAnchor, constant: Constants.titleTopAnchor),
            title.leadingAnchor.constraint(equalTo: userStatusView.leadingAnchor, constant: Constants.titleLeadingAnchor)
        ])
    }
    
    private func setupSubtitle() {
        subtitle.font = UIFont.boldSystemFont(ofSize: Constants.subtitleFontSize)
        
        container.addSubview(subtitle)
        subtitle.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            subtitle.topAnchor.constraint(equalTo: title.bottomAnchor, constant: Constants.subtitleTopAnchor),
            subtitle.leadingAnchor.constraint(equalTo: userStatusView.leadingAnchor, constant: Constants.subtitleLeadingAnchor),
        ])
    }
    
    private func setupImage(_ withUrl: String) {
        webImageManager.getImage(url: withUrl) { [weak self] result in
            guard let self else { return }
            switch result {
            case let .success(image):
                self.userAvatar.image = image
            case let .failure(failure):
                print(failure)
            }
        }
    }
}
