//
//  UserOnlineStatus.swift
//  Steam Friends
//
//  Created by Роман Степанов on 09.04.2024.
//

import Foundation

public enum UserStatus {
    case online
    case offline
    case isPlaying
}
