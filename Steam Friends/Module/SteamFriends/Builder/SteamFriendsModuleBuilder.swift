//
//  SteamFriendsModuleBuilder.swift
//  Steam Friends
//
//  Created by Роман Степанов on 08.04.2024.
//

import Foundation

final class SteamFriendsModuleBuilder {
    
    static func build(model: AuthenticateModel) -> SteamFriendsViewController {
        let interactor = SteamFriendsInteractor(model: model)
        let presenter = SteamFriendsPresenter(interactor: interactor)
        let viewController = SteamFriendsViewController(output: presenter)
        presenter.view = viewController
        
        return viewController
    }
}
