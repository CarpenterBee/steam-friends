//
//  WebImageManager.swift
//  Steam Friends
//
//  Created by Роман Степанов on 09.04.2024.
//

import Foundation
import UIKit

public protocol WebImageServiceProtocol {
    func getImage(url UrlStr: String, completion: @escaping (Result<UIImage, Error>) -> Void)
}

public final class WebImageService: WebImageServiceProtocol {
    
    private enum Errors: Error {
        case invalidURLImage
        case invalidStateImage
    }
    
    private let urlSession = URLSession(configuration: .ephemeral)
    
    public init() { }
    
    public func getImage(url UrlStr: String, completion: @escaping (Result<UIImage, Error>) -> Void) {
       guard let url = URL(string: UrlStr) else {
           completion(.failure(Errors.invalidURLImage))
           return
       }
       urlSession.dataTask(with: url) { data, response, error in
           DispatchQueue.main.async {
               switch(data, error) {
               case let (.some(data), nil ):
                   guard let image = UIImage(data: data) else { return }
                   completion(.success(image))
               case let (nil, .some(error)):
                   completion(.failure(error))
               default:
                   completion(.failure(Errors.invalidStateImage))
               }
           }
       }.resume()
   }
}
