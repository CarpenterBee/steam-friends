//
//  FriendsService.swift
//  Steam Friends
//
//  Created by Роман Степанов on 10.04.2024.
//

import Foundation
import UIKit

public protocol FriendsServiceProtocol {
    func getFriends(userModel: AuthenticateModel, completion: @escaping ([Player]) -> Void)
    
    func getProfileImage(userModel: AuthenticateModel, completion: @escaping (UIImage) -> Void)
}

public final class FriendsService: FriendsServiceProtocol {
    
    // MARK: - Private Properties
    
    private let networkManager: NetworkManagerProtocol
    
    private let webImageService: WebImageServiceProtocol
    
    // MARK: - Init
    
    public init(networkManager: NetworkManagerProtocol = NetworkManager(),
                webImageService: WebImageServiceProtocol = WebImageService()) {
        self.networkManager = networkManager
        self.webImageService = webImageService
    }
    
    // MARK: - Public Properties
    
    public func getFriends(userModel: AuthenticateModel, completion: @escaping ([Player]) -> Void) {
        let group = DispatchGroup()

        var friendsIDs: [String] = []
        group.enter()
        networkManager.getRequest(with: .friendsList,
                                  model: Friendslist.self,
                                  queryItems: [
                                    .init(name: "key", value: userModel.key),
                                    .init(name: "steamid", value: userModel.id),
                                    .init(name: "relationship", value: "friend")
                                  ]) { result in
            switch result {
            case let .success(friends):
                friendsIDs = friends.friendslist.friends.map { $0.steamid }
            case let .failure(failure):
                print(failure)
            }
            group.leave()
        }
        
        group.notify(queue: .global(qos: .userInitiated)) {
            let group = DispatchGroup()
            
            var friends: [[Player]] = []
            for id in friendsIDs {
                group.enter()
                self.networkManager.getRequest(with: .userInfo,
                                               model: UserInfo.self,
                                               queryItems: [
                                                .init(name: "key", value: "7FF409AE803700BD9F8ED066C6AE7A8A"),
                                                .init(name: "steamids", value: id),
                                               ]) { result in
                                                   switch result {
                                                   case let .success(friend):
                                                       friends.append(friend.users.players)
                                                   case let .failure(failure):
                                                       print(failure)
                                                   }
                                                   group.leave()
                                               }
            }
            group.notify(queue: .main) {
                completion(friends.flatMap { $0 })
            }
        }
    }
    
    public func getProfileImage(userModel: AuthenticateModel, completion: @escaping (UIImage) -> Void) {
        let group = DispatchGroup()
        var iconURL: String?
        group.enter()
        self.networkManager.getRequest(with: .userInfo,
                                       model: UserInfo.self,
                                       queryItems: [
                                        .init(name: "key", value: userModel.key),
                                        .init(name: "steamids", value: userModel.id),
                                       ]) { result in
                                           switch result {
                                           case let .success(user):
                                               iconURL = user.users.players.first?.avatar
                                           case let .failure(failure):
                                               print(failure)
                                           }
                                           group.leave()
                                       }
        group.notify(queue: .main) { [weak self] in
            guard let iconURL else { return }
            self?.webImageService.getImage(url: iconURL) { result in
                switch result {
                case let .success(image):
                    completion(image)
                case let .failure(error):
                    print(error)
                }
            }
        }
    }
}

extension URLRequest {
    static var empty = URLRequest(url: URL(string: "╲/╭[☉﹏☉]╮/╱")!)
}
