//
//  NetworkManager.swift
//  Steam Friends
//
//  Created by Роман Степанов on 08.04.2024.
//

import Foundation

public protocol NetworkManagerProtocol {
    func getRequest<T: Decodable>(with endpoint: Endpoint,
                                  model: T.Type,
                                  queryItems: [URLQueryItem],
                                  completion: @escaping (Result<T, Error>) -> Void)
}

public final class NetworkManager: NetworkManagerProtocol {
    
    private enum Errors: Error {
        case invalidURL
        case invalidState
    }
    
    private let urlSession = URLSession(configuration: .ephemeral)
    private let jsonDecoder = JSONDecoder()
    
    public init() { }
    
    public func getRequest<T: Decodable>(with endpoint: Endpoint,
                                         model: T.Type,
                                         queryItems: [URLQueryItem] = [],
                                         completion: @escaping (Result<T, Error>) -> Void) {
        urlSession.dataTask(with: endpoint.request(queryItems: queryItems) ?? .empty) { data, response, error in
            DispatchQueue.main.async {
                switch(data, error) {
                case let (.some(data), nil):
                    do {
                        let result = try self.jsonDecoder.decode(model, from: data)
                        completion(.success(result))
                    } catch {
                        completion(.failure(error))
                    }
                case let (nil, .some(error)):
                    completion(.failure(error))
                default:
                    completion(.failure(Errors.invalidState))
                }
            }
        }.resume()
    }
}
