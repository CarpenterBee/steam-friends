//
//  Endpoint.swift
//  Steam Friends
//
//  Created by Роман Степанов on 08.04.2024.
//

import Foundation

public enum Endpoint: String {
    
    private struct Constants {
        static let scheme = "https"
        static let baseURL = "api.steampowered.com"
    }

    case friendsList = "/ISteamUser/GetFriendList/v0001/"
    
    case userInfo = "/ISteamUser/GetPlayerSummaries/v0002/"
    
    public func request(queryItems: [URLQueryItem]) -> URLRequest? {
        let enrichedUrl = self.url?.appending(queryItems: queryItems)
        guard let url = enrichedUrl else { return nil }
        var request = URLRequest(url: url)
        request.httpMethod = self.httpMethod
        return request
    }
    
    private var url: URL? {
        var components = URLComponents()
        components.scheme = Constants.scheme
        components.host = Constants.baseURL
        components.path = self.path

        return components.url
    }
    
    private var path: String {
        switch self {
        case .friendsList: self.rawValue
        case .userInfo: self.rawValue
        }
    }
    
    private var httpMethod: String {
        switch self {
        case .friendsList,
             .userInfo: HTTP.Method.get.rawValue
        }
    }
}
