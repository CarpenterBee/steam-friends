//
//  AppDelegate.swift
//  Steam Friends
//
//  Created by Роман Степанов on 08.04.2024.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        
        let servicesModule = AuthenticateModuleBuilder.build()
        let naviationController = UINavigationController(rootViewController: servicesModule)
        naviationController.setupBarColor()
        window?.rootViewController = naviationController
        
        return true
    }
}

