# Steam Friends 

## Описание

* Приложение позволяет посмотреть свой список друзей Steam
* Платформа: IOS

## Скриншоты

<img src="Screenshots/Authenticate.png" width="197" height="426">
<img src="Screenshots/SteamFriends.png" width="197" height="426">

## Технологии

* UI - UIKit: UINavigationController, UITabelView
* Выпилен UIStoryboard
* Верстка - code + NSLayoutConstraints
* Архитектура - VIPER
* Переиспользуемый сетевой слой (Generics) - URLcomponents, URLsession, Result<T, Error>
* Сохранение данных - UserDefolts 
* Паттерны: Builder

### Устройства

* IPhone
* IPad
* IOS 17.0

### Установка

* загрузить проект из GitLab

### Среда разработки

* реализованно при помощи XCode
* версия 17.2

### Инструкция по использованию

* Необходимо ввсети id и Key своего аккаунта Steam, после чего появиться список друзей
* Тестовые данные для входа: Key: 7FF409AE803700BD9F8ED066C6AE7A8A : id: 76561198174826999
 
## Разработчик

* Роман Степанов
* Почта - roma-s01@mail.ru
* Телеграм - @chester_hard

## История верий

* 1.0
